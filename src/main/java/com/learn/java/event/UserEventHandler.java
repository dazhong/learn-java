package com.learn.java.event;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author chenzq
 */
@Slf4j
@Component
public class UserEventHandler extends EventAdapter<UserEvent> {
    @Override
    public boolean process(UserEvent userEvent) {

        log.info("reslove event:{}",userEvent.toString());


        return true;
    }
}
