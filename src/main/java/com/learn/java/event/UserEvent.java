package com.learn.java.event;

import lombok.ToString;

/**
 * @author chenzq
 */
@ToString
public class UserEvent implements BaseEvent {
    public UserEvent(String name) {
        this.name = name;
    }

    private String name;

    public UserEvent() {
    }
}
