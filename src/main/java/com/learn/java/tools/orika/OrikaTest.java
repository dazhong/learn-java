package com.learn.java.tools.orika;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;

/**
 * @author chenzq
 */
public class OrikaTest {

    public static void main(String[] args) {
        DefaultMapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
        MapperFacade mapperFacade = mapperFactory.getMapperFacade();


        Student target = new Student();

        Student student = new Student();
        student.setName("李四");
        student.setScope(100);
        mapperFacade.map(student, target);
        System.out.println(target);
    }
}
