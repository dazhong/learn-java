package com.learn.java.tools.orika;

import lombok.Data;

/**
 * @author chenzq
 */
@Data
public class Teacher {
    private String name;
    private String age;
}
