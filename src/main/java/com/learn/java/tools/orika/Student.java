package com.learn.java.tools.orika;

import lombok.Data;
import lombok.ToString;

/**
 * @author chenzq
 */
@Data
@ToString
public class Student {
    private String name;
    private Integer scope;
}
