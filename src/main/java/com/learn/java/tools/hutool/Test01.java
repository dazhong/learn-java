package com.learn.java.tools.hutool;

import javax.xml.transform.Source;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * @author chenzq
 */
public class Test01 {
    public static void main(String[] args) throws IOException {
        Path path = Paths.get(".", "prohect", "studt");
        System.out.println(path);
        System.out.println(path.toAbsolutePath());
        System.out.println(path.normalize());
        System.out.println(path.toFile());
        for (Path path1 : path) {
            System.out.println(path1);
        }
        LocalDateTime now = LocalDateTime.now();
        System.out.println(now);
        System.out.println(LocalDate.now());
        System.out.println(LocalTime.now());
        LocalDateTime now1 = LocalDateTime.now();
        System.out.println(now1.toLocalDate());
        System.out.println(now1.toLocalTime());
        LocalDate l1 = LocalDate.of(2020, 8, 6);
        LocalTime l2 = LocalTime.of(17, 46, 11);
        System.out.println(LocalDateTime.of(l1, l2));


    }
}
