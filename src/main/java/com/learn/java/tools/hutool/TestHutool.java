package com.learn.java.tools.hutool;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.io.resource.ClassPathResource;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.core.util.StrUtil;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * @author chenzq
 */
@Slf4j
public class TestHutool {
    public static void main(String[] args) throws IOException {
        Person person = new Person();
        person.setUserName("张三");
        person.setAge(19);
        Map<String, Object> stringObjectMap = BeanUtil.beanToMap(person);
        System.out.println("BeanToMap:" + stringObjectMap);
        Person mapToBean = BeanUtil.mapToBean(stringObjectMap, Person.class, false);
        log.info("map to bean:{}", mapToBean);
        Person person1 = new Person();
        BeanUtil.copyProperties(person, person1);
        log.info("person1:{}", person1);


        String dateStr = "2020-08-01";
        String[] strArr = {"a", "b", "c", "d"};
        List<?> objects = Convert.toList(strArr);
        List<?> objects1 = Convert.toList(dateStr);
        System.out.println("arr to list:" + objects);
        System.out.println("str to list:" + objects1);
        String template = "这只是个占位符:{}";
        String str2 = StrUtil.format(template, "我是占位符");
        log.info("/strUtil format:{}", str2);

        ClassPathResource resource = new ClassPathResource("application.properties");
        Properties properties = new Properties();
        properties.load(resource.getStream());
        log.info("/classPath:{}", properties);

        Method getPrivate = ReflectUtil.getMethod(Person.class, "getPrivate");
        Class<?> returnType = getPrivate.getReturnType();
        System.out.println(returnType);
        Person instance = ReflectUtil.newInstance(Person.class);

        Object getPrivate1 = ReflectUtil.invoke(instance, "getPrivate");
        System.out.println(getPrivate1);

        String n3 = "1.234";
//判断是否为数字、整数、浮点数
        System.out.println(NumberUtil.isNumber(n3));
        System.out.println(NumberUtil.isInteger(n3));
        System.out.println(NumberUtil.isDouble(n3));

//数组转换为列表
        String[] array = new String[]{"a", "b", "c", "d", "e"};
        List<String> list = CollUtil.newArrayList(array);
//join：数组转字符串时添加连接符号
        String joinStr = CollUtil.join(list, ",");
        log.info("collUtil join:{}", joinStr);


    }
}

