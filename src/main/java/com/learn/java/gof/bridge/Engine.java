package com.learn.java.gof.bridge;

/**
 * 汽车引擎接口
 * @author chenzq
 */
public interface Engine {
    void Start();
}
