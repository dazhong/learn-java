package com.learn.java.gof.bridge;

import lombok.extern.slf4j.Slf4j;

/**
 * “修正”抽象类 定义额外操作
 * @author chenzq
 */
@Slf4j
public abstract class ReginedCar extends Car  {

    public ReginedCar(Engine engine) {
        super(engine);
    }

    @Override
    public void drive() {
        this.engine.Start();
        System.out.println("Drive " + getBrand() + " car...");
    }

    protected abstract String getBrand();
}
