package com.learn.java.gof.bridge;

/**
 * 奥迪车
 * @author chenzq
 */
public class AudiCar extends ReginedCar {
    public AudiCar(Engine engine) {
        super(engine);
    }

    @Override
    protected String getBrand() {
        return "Audi";
    }
}
