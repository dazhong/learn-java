package com.learn.java.gof.bridge;

/**
 * @author chenzq
 */
public class AudiEngine implements Engine {
    @Override
    public void Start() {
        System.out.println("Start Audi Engine...");
    }
}
