package com.learn.java.gof.bridge;

/**
 * @author chenzq
 */
public class DriveCar {
    public static void main(String[] args) {
        AudiCar audiCar = new AudiCar(new AudiEngine());
        audiCar.drive();
    }
}
