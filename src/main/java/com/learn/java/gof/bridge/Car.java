package com.learn.java.gof.bridge;

/**
 * 声明汽车抽象类
 * @author chenzq
 */
public abstract class Car {

    protected Engine engine;

    public Car(Engine engine) {
        this.engine = engine;
    }

    public abstract void drive();
}
