package com.learn.java.controller;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.CircleCaptcha;
import cn.hutool.captcha.LineCaptcha;
import cn.hutool.captcha.ShearCaptcha;
import cn.hutool.core.convert.Convert;
import com.learn.java.event.EventBusFacade;
import com.learn.java.event.TestEvent;
import com.learn.java.event.UserEvent;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author chenzq
 */
@RestController
public class HutoolsController {

    @GetMapping("/captchaUtil")
    public void captchaUtil(HttpServletRequest request, HttpServletResponse response) {
        //生成验证码图片
        LineCaptcha circleCaptcha = CaptchaUtil.createLineCaptcha(200, 100, 4, 500);
        try {
            request.getSession().setAttribute("CAPTCHA_KEY", circleCaptcha.getCode());
            response.setContentType("image/png");//告诉浏览器输出内容为图片
            response.setHeader("Pragma", "No-cache");//禁止浏览器缓存
            response.setHeader("Cache-Control", "no-cache");
            response.setDateHeader("Expire", 0);
            circleCaptcha.write(response.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @GetMapping("event")
    public void event() {
        for (int i = 0; i < 1000; i++) {
            String toStr = Convert.toStr(i);
//            EventBusFacade.execute(new TestEvent(toStr)); //发布事件
            EventBusFacade.post(new UserEvent(toStr));
        }

    }
}
